#!/usr/bin/perl
#
# Graph site counts from SiteSummary
#
# Magick markers (optional):
#%# family=auto
#%# capabilities=autoconf

use strict;
use warnings;

use SiteSummary;

my %sitelabels;

if (!$ARGV[0]) {
    for_all_hosts(\&handle_host);
    # List values in the opposit order of the configuration order, to
    # try to get the same order on the graphs and the values.
    for my $sitelabel (sort { $b cmp $a } keys %sitelabels) {
        my $key = label2key($sitelabel);
        print "$key.value ", $sitelabels{$sitelabel}, "\n";
    }
} elsif ($ARGV[0] eq "config") {
    for_all_hosts(\&handle_host);
    print "graph_title SiteSummary History\n";
    print "graph_order " . join(" ", sort keys %sitelabels), "\n";
    print "graph_vlabel count\n";
    print "graph_scale yes\n";
    print "graph_height 400\n";
    print "graph_category SiteSummary\n";

    my $first = 1;
    for my $sitelabel (sort keys %sitelabels) {
        my $key = label2key($sitelabel);
        print "$key.label $sitelabel\n";
        if ($first) {
            print "$key.draw AREA\n";
        } else {
            print "$key.draw STACK\n";
        }
        $first = 0;
    }
} elsif ($ARGV[0] eq "autoconf") {
    # This module is only available when the sitesummary collector is
    # installed too, thus we always answer yes.
    print "yes\n";
    exit 0;
}

sub label2key {
    my $label = shift;
    # Clean using method described on
    # http://munin-monitoring.org/wiki/notes_on_datasource_names
    $label =~ s/^[^A-Za-z_]/_/;
    $label =~ s/[^A-Za-z0-9_]/_/g;

    return "$label";
}

sub handle_host {
    my $hostid = shift;
    for my $site (get_site($hostid)) {
        $site = "SiteMissing" unless defined $site;
        $sitelabels{$site}++;
    }
}

exit 0;
