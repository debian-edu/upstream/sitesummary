#!/bin/sh
#
# Author: Petter Reinholdtsen

set -e

daylimit=120
makewebreport=/usr/sbin/sitesummary-makewebreport
nodes=/usr/sbin/sitesummary-nodes

[ -f /etc/sitesummary/collector.cfg ] && . /etc/sitesummary/collector.cfg

# Exit imediately if the package is removed but nor purged
if [ ! -x $nodes ] ; then
    exit 0
fi

# The storage area is not configurable, because too many scripts have
# it hardcoded
entriesdir=/var/lib/sitesummary/entries

remove_old_entries() {
    find $entriesdir/. -mindepth 1 -maxdepth 1 -type d \
	-daystart -mtime +$daylimit \
	-exec /usr/lib/sitesummary/expire-entry '{}' \;
}

[ -d $entriesdir ] && remove_old_entries

if [ -x /usr/sbin/sitesummary-update-nagios ] ; then
    /usr/sbin/sitesummary-update-nagios
fi

if [ -x /usr/sbin/sitesummary-update-munin ] ; then
    /usr/sbin/sitesummary-update-munin
fi

# Update the web report once a day
[ -x $makewebreport ] && nice $makewebreport
