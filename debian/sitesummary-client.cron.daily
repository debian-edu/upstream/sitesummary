#!/bin/sh
#
# Author: Petter Reinholdtsen

if [ ! -x /usr/sbin/sitesummary-client ] || \
    [ -d /run/systemd/system ]; then
    exit 0
fi

# Read the package default.  Make sure this is identical to the code
# in sitesummar-client
[ -f /usr/share/sitesummary/sitesummary-client.conf ] && \
  . /usr/share/sitesummary/sitesummary-client.conf
for confdir in \
    /usr/share/sitesummary/config.d \
    /etc/sitesummary/config.d
do
    [ -d $confdir ] || continue
    for config in $confdir/* ; do
	[ -f $config ] && . $config
    done
done

# Sleep a random number of seconds to avoid all clients connecting to
# the server at the same time.  Based on code from the cron-apt
# package.
if [ -n "$runsleep" ] ; then
    if [ $runsleep -gt 0 ] ; then
        if [ -z "$RANDOM" ] ; then
            # A fix for shells that do not have this bash feature.
            RANDOM=$(dd if=/dev/urandom count=1 2> /dev/null | cksum | cut -c"1-5")
        fi
        TIME=$(($RANDOM % $runsleep))
        sleep $TIME
    fi
fi

# Run once a day to report the whereabouts of the machine
nice /usr/sbin/sitesummary-client
