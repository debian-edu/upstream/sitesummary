#!/usr/bin/perl
# check_kernel_status : check if the running kernel is the latest installed
# By Toni Van Remortel [toni.van.remortel@p-ops.be]
# 2008-07-28
# GPLv2
# Downloaded from
# http://www.monitoringexchange.org/attachment/download/Check-Plugins/Operating-Systems/Linux/Running-kernel-compared-to-installed-kernel-version/check_kernel_status

use strict; 
use warnings;

my $OK = 0;
my $WARN = 1;
my $CRIT = 2;
my $UNKN = 3;

my $sig; 
my @running_version;
my @installed_version; 

# First, find the current running kernel version
if ( -e '/proc/version_signature' )
{
	# Likely Ubuntu
	$sig = `cat /proc/version_signature`;
	if ( $sig =~ /.* (\d+)\.(\d+)\.(\d+)-(\d+)\.(\d+)-[generic|server]/ )
	{
		@running_version = ($1, $2, $3, $4, $5, 0);
	}
	else
	{
		print "UNKNOWN - Cannot find running Ubuntu kernel version\n";
		exit $UNKN;
	}
}
elsif ( -e '/proc/version' )
{
	# Likely Debian
	$sig = `cat /proc/version`;
	if (
             # New format in kernel package version 3.2.32-1, match first to also work with
             # kernel 3.13 and later in Debian.
             $sig =~ / Debian (\d+)\.(\d+)\.(\d+)-(\d+)/
	     || $sig =~ / Debian (\d+)\.(\d+)\.(\d+)-ckt(\d+)-(\d+)/

             || $sig =~ /\(Debian (\d+)\.(\d+)\.(\d+)\.dfsg\.(\d+)-(\d+)\)/
	     || $sig =~ /\(Debian (\d+)\.(\d+)\.(\d+)\.dfsg\.(\d+)-(\d+)\w+(\d+)\)/
	     || $sig =~ /\(Debian (\d+)\.(\d+)\.(\d+)-(\d+).+?(\d+).+?(\d+)\)/
	     || $sig =~ /\(Debian (\d+)\.(\d+)\.(\d+)-(\d+)lenny(\d+)\)/

	   )
	{
		@running_version = ($1, $2, $3, $4, $5 || 0, $6 || 0);
	}
	else
	{
		print "UNKNOWN - Cannot find running Debian kernel version\n";
		exit $UNKN;
	}
}
else
{
	print "UNKNOWN - Cannot extract running kernel info\n";
	exit $UNKN;
}

# Next, find the installed kernel version
# Yes, as you can see, it is limited to 2.6 and 3.X kernels here.
# But I assume that you don't need reboots anymore when this major
# version has passed.
my $dpkg_list = `COLUMNS=1024 dpkg -l`;
my $dpkg;
for my $line (split("\n", $dpkg_list)) {
	chomp $line;
	$dpkg = $line if ($line =~ m/^ii.+linux-image-(2.6|3.\d|4.\d|5.\d)/);
}

# Now, which OS is it, and which footprint do they use?
if ( $dpkg =~ /(\d+)\.(\d+)\.(\d+)-(\d+)\.(\d+)/ )
{
	# Ubuntu
	@installed_version = ($1, $2, $3, $4, $5, 0);
}
elsif ( $dpkg =~ /(\d+)\.(\d+)\.(\d+)\.dfsg\.(\d+)-(\d+)\w+(\d+)/ )
{
	# Debian Etch and older
	@installed_version = ($1, $2, $3, $4, $5, $6);
}
elsif ( $dpkg =~ /(\d+)\.(\d+)\.(\d+)\.dfsg\.(\d+)-(\d+) / )
{
	# Debian Etch and older
	@installed_version = ($1, $2, $3, $4, $5, 0);
}
elsif ( $dpkg =~ /(\d+)\.(\d+)\.(\d+)-(\d+)\~.+?(\d+).+?(\d+)/ )
{
	# Debian Etch and older
	@installed_version = ($1, $2, $3, $4, $5, $6);
}
elsif ( $dpkg =~ /Debian (\d+)\.(\d+)\.(\d+)\+(\d+)\+lenny(\d+)/ )
{
	# Debian Lenny
	@installed_version = ($1, $2, $3, $4, $5, 0);
}
elsif ( $dpkg =~ /(\d+)\.(\d+)\.(\d+)-(\d+)lenny(\d+)/ )
{
	# Debian Lenny
	@installed_version = ($1, $2, $3, $4, $5, 0);
}
elsif ( $dpkg =~ / (\d+)\.(\d+)\.(\d+)-(\d+)/ )
{
	# Debian Lenny
	@installed_version = ($1, $2, $3, $4, 0, 0);
}
elsif( $dpkg =~ / (\d+)\.(\d+)\.(\d+)-ckt(\d+)-(\d+)/ )
{
	@installed_version = ($1, $2, $3, $4, $5, 0);
}
else
{
	print "UNKNOWN - Could not determine installed version ($dpkg).\n";
	exit $UNKN;
}

# Calculate sums for easy comparison
my $running_version_sum = sprintf("%02d%02d%02d%02d%02d%02d", @running_version);
my $installed_version_sum = sprintf("%02d%02d%02d%02d%02d%02d", @installed_version);
# And some readable format
my $print_running_version = sprintf("%d.%d.%d-%d.%d.%d", @running_version);
my $print_installed_version = sprintf("%d.%d.%d-%d.%d.%d", @installed_version);

# Do we need a reboot?
if ( $running_version_sum < $installed_version_sum )
{
	print "WARNING - Reboot required : running kernel = $print_running_version, installed kernel = $print_installed_version\n";
	exit $WARN;
}
else
{
	print "OK - running kernel = $print_running_version, installed kernel = $print_installed_version\n";
	exit $OK;
}
