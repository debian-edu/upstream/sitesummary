#!/bin/sh
#
# Author: Petter Reinholdtsen

set -e

umask 022

nodes=/usr/sbin/sitesummary-nodes
muninopts=""

# Set MUNINDIR in collector.cfg to /etc/munin/ to automatically add hosts to
# the default munin configuration via include files.
[ -f /etc/sitesummary/collector.cfg ] && . /etc/sitesummary/collector.cfg

# Default to /var/lib/sitesummary and canonicalize path.
MUNINDIR="$(realpath -mqs "${MUNINDIR:-/var/lib/sitesummary}")"

# The storage area is not configurable, because too many scripts have
# it hardcoded
entriesdir=/var/lib/sitesummary/entries

generate_munin_config() {
    # Generate munin config.  Edit /etc/cron.d/munin to enable it.
    # Add -c /var/lib/sitesummary/munin.conf to the calls to the munin scripts,
    # or change MUNINDIR as described above.

    mkdir -p "${MUNINDIR}/munin-conf.d"
    chmod a+rx "${MUNINDIR}/munin-conf.d"

    if [ ! -f "${MUNINDIR}/munin.conf" ]; then
        cat > "${MUNINDIR}/munin.conf" <<EOF
# (Exactly one) directory to include all files from.
#
includedir "${MUNINDIR}/munin-conf.d"
EOF
        chown root:root "${MUNINDIR}/munin.conf"
    fi

    (
        cat <<EOF
# Munin server configuration generated from cron using sitesummary
# data by $0
# Do not edit, it will be overwritten.
EOF

        $nodes -m $muninopts
    ) >"${MUNINDIR}/munin-conf.d/sitesummary.conf" &&
        chown root:root "${MUNINDIR}/munin-conf.d/sitesummary.conf"
}

# Only enable if munin and sitesummary is installed.
if [ -f /etc/munin/munin.conf ] && [ -x /usr/sbin/sitesummary-nodes ]; then
    generate_munin_config
fi
